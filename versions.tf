terraform {
  required_version = ">= 0.14.0"

  required_providers {
    local = ">= 1.2"
  }
  
  experiments = [module_variable_optional_attrs]
}
